import { Injectable } from '@angular/core';
import {User} from '../user';
import {HttpClient, HttpHeaders} from "@angular/common/http";
@Injectable({
  providedIn: 'root'
})
export class UserdataService {
  userAPI = "https://jsonplaceholder.typicode.com/users"
  constructor(private http: HttpClient) { }
  getUsers(){
    return this.http.get<User[]>(this.userAPI, this.httpOptions);
  }
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
}
