import { Injectable } from '@angular/core';
import { Hero } from '../hero';
import { Heroes } from '../mock-heroes';
import {Observable, of} from 'rxjs';
import {MessagesService} from './messages.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  constructor(
    private http : HttpClient,
    private messageService : MessagesService
    ) { }
  private heroesUrl = 'api/heroes';
  private log(message: string): void{
    this.messageService.add("HeroService" + message);
  }
 private weatherUrl = 'https://api.openweathermap.org/data/2.5/forecast?q=Ahmadabad&units=metric&APPID=ad617f2b28d6eafb5c897fc98f544c2f'; 
  // getWeather(){
  //   return this.http.get(this.weatherUrl).pipe(
  //     tap(_=>this.log(" List Displayed")),
  //     catchError(this.handleError(get', []))
  //   );
  // }
  getHeroes() : Observable<Hero[]>{
    return this.http.get<Hero[]>(this.heroesUrl)
    .pipe(
      tap(_=>this.log(" List Displayed")),
      catchError(this.handleError<Hero[]>('getHeroes', []))
    );
  }
  getHero(id:number) : Observable<Hero>{
    const url = `${this.heroesUrl}/${id}`;
    return this.http.get<Hero>(url).pipe(
      tap(_=> this.log(": Displayed Hero: "+ id)),
      catchError(this.handleError<Hero>("getHero id=${id}"))
    );
  }
  updateHero(hero: Hero): Observable<any>{
    return this.http.post<any>(this.heroesUrl, hero, this.httpOptions).pipe(
      tap(_=>this.log(` Updated content at : ${hero.id}`)),
      catchError(this.handleError<any>('updateHero'))
    )
  }
  addHero (hero: Hero): Observable<Hero> {
    return this.http.post<Hero>(this.heroesUrl, hero, this.httpOptions).pipe(
      tap((newHero: Hero) => this.log(`added hero w/ id=${newHero.id}`)),
      catchError(this.handleError<Hero>('addHero'))
    );
  }
  deleteHero (hero: Hero | number): Observable<Hero> {
    const id = typeof hero === 'number' ? hero : hero.id;
    const url = `${this.heroesUrl}/${id}`;
  
    return this.http.delete<Hero>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted hero id=${id}`)),
      catchError(this.handleError<Hero>('deleteHero'))
    );
  }
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
